import datetime
import pendulum

from airflow import DAG
from airflow.operators.python import PythonOperator
from src.kalameet.tasks.api.yahoo_finance import fetch_data
from pathlib import Path

from src.kalameet.tasks.airflow_influx import get_last_entry_from_measurement, write_record

RAW_PATH = Path('/home/nuno/Documents/Projects/Kalameet/data/01_raw')

TOKEN = "za578-mXS3Lh7BYjJlHUy1GlsHiKlMub7mdXcE9CclwXcEAzKvSr01RdmFwOcIe0HqjP2t8sKkeKgwfZCUY9hQ=="
ORG = "Kalameet"
BUCKET = "stock"
MEASUREMENT = "nvda"
STOCK = "NVDA"

def wrap_extract(ti):
    
    fetched_data = ti.xcom_pull(key='return_value', task_ids='get_last_entry_from_measurement') 
    return fetch_data(stock=STOCK, start=fetched_data)


with DAG(
    dag_id='data_extraction',
    schedule_interval=datetime.timedelta(hours=24),
    start_date=pendulum.datetime(2021, 1, 1, tz="UTC"),
    catchup=False,
    tags=['kalameet']
) as dag:

    get_last_entry = PythonOperator(
        task_id='get_last_entry_from_measurement',
        python_callable=get_last_entry_from_measurement,
        op_kwargs={
            "bucket": BUCKET,
            "measurement": MEASUREMENT,
            "org": ORG,
            "token": TOKEN
        }
    )

    fetch_stock_data = PythonOperator(
        task_id='fetch_data',
        python_callable=wrap_extract
    )

    write_stock_data = PythonOperator(
        task_id='write_data',
        python_callable=write_record,
        op_kwargs={
            "load_path": RAW_PATH.joinpath(f"{STOCK}.csv"),
            "bucket": BUCKET,
            "measurement": MEASUREMENT,
            "org": ORG,
            "token": TOKEN
        }
    )

    get_last_entry >> fetch_stock_data >> write_stock_data