import pandas as pd
import numpy as np
import argparse
import mlflow

from kalameet.models.dqn.agent import DQNAgent
from kalameet.models.dqn.trainer import Trainer
from kalameet.models.dqn.environment import KalameetStockEnv

from tf_agents.environments import tf_py_environment
from pathlib import Path

MODEL_INPUT = Path('/home/nuno/Documents/Projects/Kalameet/data/05_model_input')

def load_data(data_path: str):

    df = pd.read_csv(MODEL_INPUT.joinpath(data_path), comment='#')

    return df['Close'].to_numpy(dtype=np.float32)

def train(data_path,
    window_size,
    initial_money,
    update_step,
    discount,
    learning_rate,
    replay_buffer_max_size,
    num_iterations,
    num_evals,
    initial_collect_steps,
    collect_steps,
    log_interval,
    eval_interval,
    batch_size):

    stock_data = load_data(data_path=data_path)

    train_environment = KalameetStockEnv(
        stock_data=stock_data, 
        lag_size=window_size, 
        discount=discount,
        initial_money=initial_money)

    eval_environment = KalameetStockEnv(
        stock_data=stock_data, 
        lag_size=window_size, 
        discount=discount,
        initial_money=initial_money)

    train_environment_tf = tf_py_environment.TFPyEnvironment(train_environment)
    eval_environment_tf = tf_py_environment.TFPyEnvironment(eval_environment)

    agent = DQNAgent(learning_rate=learning_rate, env=train_environment_tf, n_step_update=update_step)

    trainer = Trainer(
        model=agent,
        train_env=train_environment,
        eval_env=eval_environment_tf,
        n_step_update=update_step,
        replay_buffer_max_size=replay_buffer_max_size)

    trainer.train(
        num_iterations=num_iterations,
        num_eval_episodes=num_evals,
        collect_steps_per_iteration=collect_steps,
        initial_collect_steps=initial_collect_steps,
        log_interval=log_interval,
        eval_interval=eval_interval,
        batch_size=batch_size)

if __name__=='__main__':

    mlflow.set_experiment('kalameet-bot')
    mlflow.set_tracking_uri('sqlite:///mlflow.db')

    parser = argparse.ArgumentParser()

    parser.add_argument('-d, --data_path', type=str, dest='data_path')
    parser.add_argument('-m, --inital_money', type=float, dest='initial_money')
    parser.add_argument('-w, --window_size', type=int, dest='window_size')
    parser.add_argument('--update_step', type=int, dest='update_step', default=2)
    parser.add_argument('--discount', type=float, dest='discount', default=1.0)
    parser.add_argument('--learning_rate', type=float, dest='learning_rate', default=1e-3)
    parser.add_argument('--replay_buffer_length', type=int, dest='replay_buffer_length', default=100000)
    parser.add_argument('-i, --num_iterations', type=int, dest='num_iterations', default=10000)
    parser.add_argument('-e, --num_evals', type=int, dest='num_evals', default=10)
    parser.add_argument('--collect_steps', type=int, dest='collect_steps', default=1)
    parser.add_argument('--initial_collect_steps', type=int, dest='initial_collect_steps', default=1000)
    parser.add_argument('--log_interval', type=int, dest='log_interval', default=200)
    parser.add_argument('--eval_interval', type=int, dest='eval_interval', default=1000)
    parser.add_argument('--batch_size', type=int, dest='batch_size', default=64)

    args = parser.parse_args()

    data_path = args.data_path
    window_size = args.window_size
    initial_money = args.initial_money
    update_step = args.update_step
    discount = args.discount
    learning_rate = args.learning_rate
    replay_buffer_max_size = args.replay_buffer_length
    num_iterations = args.num_iterations
    num_evals = args.num_evals
    initial_collect_steps = args.initial_collect_steps
    collect_steps = args.collect_steps
    log_interval = args.log_interval
    eval_interval = args.eval_interval
    batch_size = args.batch_size

    
    train(
        data_path,
        window_size,
        initial_money,
        update_step,
        discount,
        learning_rate,
        replay_buffer_max_size,
        num_iterations,
        num_evals,
        initial_collect_steps,
        collect_steps,
        log_interval,
        eval_interval,
        batch_size
    )