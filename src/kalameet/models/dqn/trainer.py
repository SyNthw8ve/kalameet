import numpy as np
import mlflow
import tensorflow as tf

from datetime import datetime
from kalameet.models.dqn.replay_buffer import ReplayBuffer

from tf_agents.utils import common
from tf_agents.environments import tf_py_environment
from tf_agents.policies import py_tf_eager_policy
from tf_agents.drivers import py_driver
from tf_agents.policies import random_tf_policy
from tf_agents.trajectories import trajectory
from tf_agents.policies import policy_saver

from tqdm import tqdm

class Trainer:

    def __init__(self,
                 model,
                 train_env: tf_py_environment.TFPyEnvironment,
                 eval_env: tf_py_environment.TFPyEnvironment,
                 n_step_update: int = 2,
                 replay_buffer_max_size: int = None,
                ) -> None:

        self.agent = model.agent
    
        self.train_env: tf_py_environment.TFPyEnvironment = train_env
        self.eval_env: tf_py_environment.TFPyEnvironment = eval_env

        self.n_step_update = n_step_update
        self.replay_buffer = ReplayBuffer(
            agent=self.agent,
            max_length=replay_buffer_max_size,
            n_step_update=n_step_update)

    def compute_avg_return(self, environment, policy, num_episodes=10):

        total_return = 0.0
        for _ in range(num_episodes):

            time_step = environment.reset()
            episode_return = 0.0
            rewards = []

            while not time_step.is_last():
                action_step = policy.action(time_step)
                time_step = environment.step(action_step.action)
                episode_return += time_step.reward
                
                rewards.append(time_step.reward)
            
            print(f"Episode return: {episode_return.numpy()[0]} Mean reward: {np.mean(rewards)}")  
            total_return += episode_return

        avg_return = total_return / num_episodes
        return avg_return.numpy()[0]

    def train(self, num_iterations: int, 
                num_eval_episodes: int, 
                initial_collect_steps: int,
                collect_steps_per_iteration: int,
                log_interval: int, 
                eval_interval: int,
                batch_size: int):

        self.agent.train = common.function(self.agent.train)
        self.agent.train_step_counter.assign(0)

        avg_return = self.compute_avg_return(
            self.eval_env, self.agent.policy, num_eval_episodes)

        returns = [avg_return]

        time_step = self.train_env.reset()

        random_policy = random_tf_policy.RandomTFPolicy(self.train_env.time_step_spec(),
                                                self.train_env.action_spec())

        py_driver.PyDriver(
            self.train_env,
            py_tf_eager_policy.PyTFEagerPolicy(
                random_policy, use_tf_function=True),
            [self.replay_buffer.rb_observer],
            max_steps=initial_collect_steps).run(self.train_env.reset())
            
        collect_driver = py_driver.PyDriver(
                self.train_env,
                py_tf_eager_policy.PyTFEagerPolicy(
                    self.agent.collect_policy, use_tf_function=True),
                [self.replay_buffer.rb_observer],
                max_steps=collect_steps_per_iteration
            )

        dataset = self.replay_buffer.replay_buffer.as_dataset(
            num_parallel_calls=3,
            sample_batch_size=batch_size,
            num_steps=self.n_step_update).prefetch(3)

        iterator = iter(dataset)
        current_time = datetime.now().strftime("%Y%m%d-%H%M%S")

        with mlflow.start_run():

            for epoch in range(num_iterations):

                time_step, _ = collect_driver.run(time_step)

                experience, _ = next(iterator)
                train_loss = self.agent.train(experience).loss

                step = self.agent.train_step_counter.numpy()

                if step % log_interval == 0:
                    mlflow.log_metric(key='Training Loss', value=train_loss.numpy(), step=epoch)
                    print('step = {0}: loss = {1}'.format(step, train_loss))

                if step % eval_interval == 0:
                    avg_return = self.compute_avg_return(self.eval_env, self.agent.policy, num_eval_episodes)

                    mlflow.log_metric(key='Average Return', value=avg_return, step=epoch)
                    print('step = {0}: Average Return = {1}'.format(step, avg_return))
                    returns.append(avg_return)

            tf_policy_saver = policy_saver.PolicySaver(self.agent.policy)
            
            mlflow.keras.save_model(tf_policy_saver, f'exports/models/kalameet_bot/{current_time}', keras_module='tensorflow.keras')
            mlflow.keras.log_model(tf_policy_saver, f'exports/models/kalameet_bot/{current_time}', keras_module='tensorflow.keras')

