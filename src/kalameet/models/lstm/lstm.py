import tensorflow as tf

class MultiLSTM(tf.keras.Model):

    def __init__(self, num_units, out_steps, num_features):
        super(MultiLSTM, self).__init__()

        self.lstm = tf.keras.layers.RNN(
            cell=[tf.keras.layers.LSTMCell(num_units)])

        self.W = tf.keras.layers.Dense(out_steps*num_features,
                                       kernel_initializer=tf.initializers.zeros)

        self.reshape = tf.keras.layers.Reshape([out_steps, num_features])

    def call(self, inputs):

        x = self.lstm(inputs)
        x = self.W(x)

        return self.reshape(x)
