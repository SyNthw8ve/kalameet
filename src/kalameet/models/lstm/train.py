import argparse
import pandas as pd
import mlflow

from kalameet.models.lstm.trainer import Trainer

from kalameet.models.lstm.lstm import MultiLSTM

def load_data(data_path: str):

    df = pd.read_csv(data_path, comment='#')
    df = df['Close']
    df = pd.DataFrame({'Close': df})
    
    n = len(df)
    train_df = df[0:int(n*0.7)]
    val_df = df[int(n*0.7):int(n*0.9)]
    test_df = df[int(n*0.9):]

    train_mean = train_df.mean()
    train_std = train_df.std()

    train_df = (train_df - train_mean) / train_std
    val_df = (val_df - train_mean) / train_std
    test_df = (test_df - train_mean) / train_std

    return train_df, val_df, test_df

if __name__=='__main__':

    mlflow.set_experiment('kalameet-forecast')
    mlflow.set_tracking_uri('sqlite:///mlflow.db')

    parser = argparse.ArgumentParser()

    parser.add_argument('-d, --data_path', type=str, dest='data_path')
    parser.add_argument('-i, --input_size', type=int, dest='input_size')
    parser.add_argument('-o, --output_size', type=int, dest='output_size')
    parser.add_argument('-n, --num_iterations', type=int, dest='num_iterations', default=100)
    parser.add_argument('-u, --num_units', type=int, dest='num_units', default=128)

    args = parser.parse_args()

    data_path = args.data_path
    input_size = args.input_size
    output_size = args.output_size
    num_iterations = args.num_iterations
    num_units = args.num_units
    
    train_df, val_df, test_df = load_data(data_path=data_path)

    model = MultiLSTM(num_units=num_units, out_steps=output_size, num_features=1)

    trainer = Trainer(
        train_df=train_df, 
        val_df=val_df, 
        test_df=test_df,
        input_width=input_size,
        label_width=output_size,
        label_columns=['Close'],
        model=model)
    
    trainer.train(num_iterations=num_iterations)