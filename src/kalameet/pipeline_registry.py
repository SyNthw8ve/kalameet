"""Project pipelines."""
from typing import Dict

from kedro.pipeline import Pipeline, pipeline
from kalameet.pipelines import data_extraction as de, kalameet_forecast_prediction as kfp, kalameet_bot_prediction as kbp


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from a pipeline name to a ``Pipeline`` object.
    """

    kalameet_forecast_prediction_pipeline = kfp.create_pipeline()
    kalameet_bot_prediction_pipeline = kbp.create_pipeline()

    return {
        '__default__': kalameet_forecast_prediction_pipeline,
        'kalameet_forecast': kalameet_forecast_prediction_pipeline,
        'kalameet_bot': kalameet_bot_prediction_pipeline
    }
