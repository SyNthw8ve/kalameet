from kedro.pipeline import Pipeline, node
from .nodes import get_last_entry_from_measurement, fetch_data, write_record

def create_pipeline():

    return Pipeline(nodes=[
        node(
            func=get_last_entry_from_measurement,
            inputs=[
                'params:data_extraction.bucket', 
                'params:data_extraction.measurement',
                'params:influx.org',
                'params:influx.influx_token'
            ],
            outputs='last_record_date',
            name='get_last_entry_from_measurement'
        ),
        node(
            func=fetch_data,
            inputs=[
                'params:data_extraction.stock',
                'last_record_date'
            ],
            outputs='extracted_df',
            name='fetch_data'
        ),
        node(
            func=write_record,
            inputs=[
                'extracted_df',
                'params:data_extraction.bucket', 
                'params:data_extraction.measurement',
                'params:influx.org',
                'params:influx.influx_token'
            ],
            outputs=None,
            name='write_record'
        )
    ])