import numpy as np

from kalameet.tasks.influx import get_from_date, write_bot_predictions
from kalameet.tasks.kalameet_bot.environment import simulate_environment

