import numpy as np
import tritonclient.http as httpclient

MODEL_NAME = 'kalameet_bot'

def infer_kalameet_bot(
               observations,
               discount,
               reward,
               step_type,
               headers=None,
               request_compression_algorithm=None,
               response_compression_algorithm=None):

    inputs = []
    outputs = []

    triton_client = httpclient.InferenceServerClient(
                url='localhost:8000', verbose=False)
    
    inputs.append(httpclient.InferInput('0/observation', [1, 21], "FP32"))
    inputs.append(httpclient.InferInput('0/discount', [1], "FP32"))
    inputs.append(httpclient.InferInput('0/reward', [1], "FP32"))
    inputs.append(httpclient.InferInput('0/step_type', [1], "INT32"))
    
    inputs[0].set_data_from_numpy(observations, binary_data=False)
    inputs[1].set_data_from_numpy(discount, binary_data=False)
    inputs[2].set_data_from_numpy(reward, binary_data=False)
    inputs[3].set_data_from_numpy(step_type, binary_data=False)

    outputs.append(httpclient.InferRequestedOutput('action', binary_data=False))
                         
    results = triton_client.infer(
        MODEL_NAME,
        inputs,
        outputs=outputs,
        headers=headers,
        request_compression_algorithm=request_compression_algorithm,
        response_compression_algorithm=response_compression_algorithm)

    return results.get_output('action')['data']

if __name__ == '__main__':

    observations = np.ones((1, 21), dtype=np.float32)
    discount = np.array([1], dtype=np.float32)
    reward = np.array([1], dtype=np.float32)
    step_type = np.array([0], dtype=np.int32)

    infer_kalameet_bot(
        observations=observations,
        discount=discount,
        reward=reward,
        step_type=step_type
    )