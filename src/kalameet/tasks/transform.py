import pandas as pd

from pathlib import Path

RAW_PATH = Path('/home/nuno/Documents/Projects/Kalameet/data/01_raw')
MODEL_INPUT = Path('/home/nuno/Documents/Projects/Kalameet/data/05_model_input')

def load_file(path: str):

    return pd.read_csv(RAW_PATH.joinpath(path))

def save_file(path: str, df: pd.DataFrame):

    df.to_csv(MODEL_INPUT.joinpath(path))

def retrieve_close_values(df: pd.DataFrame, name: str):

    series = df[df['_field'] == 'Close']['_value']
    
    transformed_df = pd.DataFrame({'Close': series})
    save_file(path=f"{name}.csv", df=transformed_df)

    return f"{name}.csv"